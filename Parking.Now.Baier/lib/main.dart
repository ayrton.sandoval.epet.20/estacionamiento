import 'package:email_password_login/screens/PagoAceptado.dart';
import 'package:email_password_login/screens/PagoRechazado.dart';
import 'package:email_password_login/screens/direccion.dart';
import 'package:email_password_login/screens/login_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

import 'screens/Alarma.dart';
import 'screens/DatosPage.dart';
import 'screens/Ubicación.dart';
import 'screens/add_alarm.dart';
import 'screens/home_screen.dart';
import 'screens/registration_screen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(Main());
}

class Main extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Paking Now',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      debugShowCheckedModeBanner: false,
      routes: {
        '/LoginScreen': (context) => LoginScreen(),
        '/HomeScreen': (context) => HomeScreen(),
        '/UbiScreen': (context) => UbiScreen(),
        '/RegistrationScreen': (context) => RegistrationScreen(),
        '/alarma': (context) => alarma(),
        '/AddAlarm': (context) => AddAlarm(),
        '/DatosPage': (context) => DatosPage(),
        '/direccion': (context) => direccion(),
        '/PagoAceptado': (context) => PagoAceptado(),
        '/PagoRechazado': (context) => PagoRechazado()
      },
      initialRoute: '/LoginScreen',
    );
  }
}
