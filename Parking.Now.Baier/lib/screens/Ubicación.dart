import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:email_password_login/main.dart';
import 'package:email_password_login/model/user_model.dart';
import 'package:email_password_login/screens/Alarma.dart';
import 'package:email_password_login/screens/DatosPage.dart';

import 'package:email_password_login/screens/Ubicaci%C3%B3n.dart';
import 'package:email_password_login/screens/direccion.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geocode/geocode.dart';
import 'package:map/map.dart';
import 'package:latlng/latlng.dart';
import 'login_screen.dart';
import 'direccion.dart';

class UbiScreen extends StatefulWidget {
  const UbiScreen({Key? key}) : super(key: key);

  @override
  _UbiScreenState createState() => _UbiScreenState();
}

class _UbiScreenState extends State<UbiScreen> {
  User? user = FirebaseAuth.instance.currentUser;
  UserModel loggedInUser = UserModel();

  final controller =
      MapController(location: LatLng(-38.95182191695065, -68.05918334618327));

  Offset? _dragStart;
  double _scaleStart = 1.0;
  double lat = -38.95182191695065, long = -68.05918334618327;
  String address = "";
  bool loading = false;
  void _onScaleStart(ScaleStartDetails details) {
    _dragStart = details.focalPoint;
    _scaleStart = 1.0;
  }

  void _onScaleUpdate(ScaleUpdateDetails details) {
    final scaleDiff = details.scale - _scaleStart;
    _scaleStart = details.scale;

    if (scaleDiff > 0)
      controller.zoom += 0.02;
    else if (scaleDiff < 0)
      controller.zoom -= 0.02;
    else {
      final now = details.focalPoint;
      final diff = now - _dragStart!;
      _dragStart = now;
      controller.drag(diff.dx, diff.dy);
    }
  }

  void _onScaleEnd(ScaleEndDetails details) async {
    try {
      setState(() {
        loading = true;
      });
      lat = controller.center.latitude;
      long = controller.center.longitude;
      GeoCode geocode = GeoCode();
      Address add =
          await geocode.reverseGeocoding(latitude: lat, longitude: long);
      address = (add.countryName ?? '') +
          ' - ' +
          (add.region ?? '') +
          ' - ' +
          (add.city ?? '') +
          ' - ' +
          (add.streetAddress ?? '');
    } finally {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff00406E),
        title: Text(
          'PARKING NOW',
          style: TextStyle(
            color: Colors.white,
            fontSize: 25,
            fontWeight: FontWeight.bold,
            fontFamily: 'helios',
            letterSpacing: 5,
          ),
        ),
        titleSpacing: 50,
        elevation: 10,
        centerTitle: true,
      ),
      body: Stack(
        children: [
          Align(
            alignment: Alignment.center,
            child: GestureDetector(
              onScaleStart: _onScaleStart,
              onScaleUpdate: _onScaleUpdate,
              onScaleEnd: _onScaleEnd,
              child: Map(
                controller: controller,
                builder: (context, x, y, z) {
                  final url =
                      'https://www.google.com/maps/vt/pb=!1m4!1m3!1i$z!2i$x!3i$y!2m3!1e0!2sm!3i420120488!3m7!2sen!5e1105!12m4!1e68!2m2!1sset!2sRoadmap!4e0!5m1!1e0!23i4111425';
                  return CachedNetworkImage(
                    imageUrl: url,
                    fit: BoxFit.cover,
                  );
                },
              ),
            ),
          ),
          Align(
            alignment: Alignment(0.8, 0.6),
            child: FloatingActionButton(
              onPressed: () => controller.zoom += 0.2,
              tooltip: 'Zoom +',
              child: Icon(Icons.zoom_in),
              backgroundColor: Color(0xff00406E).withOpacity(0.75),
              hoverColor: Colors.white.withOpacity(0.25),
            ),
          ),
          Align(
            alignment: Alignment(0.8, 0.9),
            child: FloatingActionButton(
              onPressed: () => controller.zoom -= 0.2,
              tooltip: 'Zoom -',
              child: Icon(Icons.zoom_out),
              backgroundColor: Color(0xff00406E).withOpacity(0.75),
              hoverColor: Colors.white.withOpacity(0.25),
            ),
          ),
          Align(
            alignment: Alignment(0.8, 0.3),
            child: FloatingActionButton(
              onPressed: () {
                Navigator.pushNamed(context, '/direccion');
              },
              tooltip: 'Formulario de Direccion',
              child: Icon(Icons.forum),
              backgroundColor: Color(0xff00406E).withOpacity(0.75),
              hoverColor: Colors.white.withOpacity(0.25),
            ),
          ),
          Align(
            alignment: Alignment(-0.8, 0.9),
            child: FloatingActionButton(
              onPressed: () {
                Navigator.pushNamed(context, '/HomeScreen');
              },
              tooltip: 'Atrás',
              child: Icon(Icons.arrow_left),
              backgroundColor: Color(0xff00406E).withOpacity(0.75),
              hoverColor: Colors.white.withOpacity(0.25),
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Icon(
              Icons.location_on,
              size: 38,
              color: Colors.red.withOpacity(0.75),
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Positioned(
              bottom: 10,
              left: (MediaQuery.of(context).size.width * 0.5) -
                  (MediaQuery.of(context).size.width * 0.37),
              child: Card(
                elevation: 12,
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.75,
                  height: 75,
                  padding: EdgeInsets.all(6),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text('Lat: $lat , Long: $long'),
                      loading ? CupertinoActivityIndicator() : Text('$address')
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  // the logout function
  Future<void> logout(BuildContext context) async {
    await FirebaseAuth.instance.signOut();
    Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => LoginScreen()));
  }
}
