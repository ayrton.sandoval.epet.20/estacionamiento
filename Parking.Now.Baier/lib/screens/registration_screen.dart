import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:email_password_login/model/user_model.dart';
import 'package:email_password_login/screens/home_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class RegistrationScreen extends StatefulWidget {
  const RegistrationScreen({Key? key}) : super(key: key);

  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  final _auth = FirebaseAuth.instance;

  // string for displaying the error Message
  String? errorMessage;

  // our form key
  final _formKey = GlobalKey<FormState>();
  // editing Controller
  final NombreEditingController = new TextEditingController();
  final ApellidoEditingController = new TextEditingController();
  final emailEditingController = new TextEditingController();
  final passwordEditingController = new TextEditingController();
  final confirmPasswordEditingController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    //first name field
    final firstNameField = TextFormField(
        style: TextStyle(color: Colors.white.withOpacity(0.80)),
        autofocus: false,
        controller: NombreEditingController,
        keyboardType: TextInputType.name,
        validator: (value) {
          RegExp regex = new RegExp(r'^.{3,}$');
          if (value!.isEmpty) {
            return ("Nombre No puede estár vacio");
            //return ("First Name cannot be Empty");
          }
          if (!regex.hasMatch(value)) {
            //return ("Enter Valid name(Min. 3 Character)");
            return ("Ingrese un nombre Válido(Minimo 3 Caracteres)");
          }
          return null;
        },
        onSaved: (value) {
          NombreEditingController.text = value!;
        },
        textInputAction: TextInputAction.next,
        decoration: InputDecoration(
          enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
            color: Colors.white.withOpacity(0.80),
            width: 1,
          )),
          prefixIcon: Icon(
            Icons.account_circle,
            color: Colors.white.withOpacity(0.80),
          ),
          contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
          labelText: ('Nombre'),
          labelStyle: TextStyle(color: Colors.white.withOpacity(0.80)),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ));

    //second name field
    final secondNameField = TextFormField(
        style: TextStyle(color: Colors.white.withOpacity(0.80)),
        autofocus: false,
        controller: ApellidoEditingController,
        keyboardType: TextInputType.name,
        validator: (value) {
          if (value!.isEmpty) {
            return ("Segundo nombre // Apellido  No puede estár vacio");
          }
          return null;
        },
        onSaved: (value) {
          ApellidoEditingController.text = value!;
        },
        textInputAction: TextInputAction.next,
        decoration: InputDecoration(
          enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
            color: Colors.white.withOpacity(0.80),
            width: 1,
          )),
          prefixIcon: Icon(
            Icons.account_circle,
            color: Colors.white.withOpacity(0.80),
          ),
          contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
          labelText: ('Segundo nombre // Apellido'),
          labelStyle: TextStyle(color: Colors.white.withOpacity(0.80)),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ));

    //email field
    final emailField = TextFormField(
        style: TextStyle(color: Colors.white.withOpacity(0.80)),
        autofocus: false,
        controller: emailEditingController,
        keyboardType: TextInputType.emailAddress,
        validator: (value) {
          if (value!.isEmpty) {
            return ("Porfavor, ingrese su email");
          }
          // reg expression for email validation
          if (!RegExp("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]")
              .hasMatch(value)) {
            return ("Porfavor, ingrese un email válido");
          }
          return null;
        },
        onSaved: (value) {
          NombreEditingController.text = value!;
        },
        textInputAction: TextInputAction.next,
        decoration: InputDecoration(
          enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
            color: Colors.white.withOpacity(0.80),
            width: 1,
          )),
          prefixIcon: Icon(
            Icons.mail,
            color: Colors.white.withOpacity(0.80),
          ),
          contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
          labelText: ('Email'),
          labelStyle: TextStyle(color: Colors.white.withOpacity(0.80)),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ));

    //password field
    final passwordField = TextFormField(
        style: TextStyle(color: Colors.white.withOpacity(0.80)),
        autofocus: false,
        controller: passwordEditingController,
        obscureText: true,
        validator: (value) {
          RegExp regex = new RegExp(r'^.{6,}$');
          if (value!.isEmpty) {
            return ("Contraseña requerida para loguearse");
            //return ("Password is required for login");
          }
          if (!regex.hasMatch(value)) {
            return ("Ingrese una contraseña válida(Minimo 6 Caracteres)");
            //return ("Enter Valid Password(Min. 6 Character)");
          }
        },
        onSaved: (value) {
          NombreEditingController.text = value!;
        },
        textInputAction: TextInputAction.next,
        decoration: InputDecoration(
          enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
            color: Colors.white.withOpacity(0.80),
            width: 1,
          )),
          prefixIcon: Icon(
            Icons.vpn_key,
            color: Colors.white.withOpacity(0.80),
          ),
          contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
          labelText: ('Contraseña'),
          labelStyle: TextStyle(color: Colors.white.withOpacity(0.80)),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ));

    //confirm password field
    final confirmPasswordField = TextFormField(
        style: TextStyle(color: Colors.white.withOpacity(0.80)),
        autofocus: false,
        controller: confirmPasswordEditingController,
        obscureText: true,
        validator: (value) {
          if (confirmPasswordEditingController.text !=
              passwordEditingController.text) {
            return "La contraseña no coincide";
          }
          return null;
        },
        onSaved: (value) {
          confirmPasswordEditingController.text = value!;
        },
        textInputAction: TextInputAction.done,
        decoration: InputDecoration(
          enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
            color: Colors.white.withOpacity(0.80),
            width: 1,
          )),
          prefixIcon: Icon(
            Icons.vpn_key,
            color: Colors.white.withOpacity(0.80),
          ),
          contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
          labelText: ('Confirmar Contraseña'),
          labelStyle: TextStyle(color: Colors.white.withOpacity(0.80)),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ));

    //signup button
    final signUpButton = Material(
      elevation: 5,
      borderRadius: BorderRadius.circular(50),
      color: Colors.white.withOpacity(0.50),
      child: MaterialButton(
          minWidth: MediaQuery.of(context).size.width,
          onPressed: () {
            signUp(emailEditingController.text, passwordEditingController.text);
          },
          child: Text(
            "Registrarse",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
            ),
          )),
    );

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Color(0xff00406E),
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white.withOpacity(0.90)),
          onPressed: () {
            // passing this to our root
            Navigator.of(context).pop();
          },
        ),
      ),
      body: Center(
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color(0xff00406E),
                  Color(0xff31AAC1),
                ]),
          ),
          padding: const EdgeInsets.all(25),
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                SizedBox(
                    child: Image.asset(
                  "assets/img/logo.png",
                  scale: 4,
                )),
                SizedBox(height: 20),
                firstNameField,
                //SizedBox(height: 45),

                SizedBox(height: 20),
                secondNameField,
                SizedBox(height: 20),
                emailField,
                SizedBox(height: 20),
                passwordField,
                SizedBox(height: 20),
                confirmPasswordField,
                SizedBox(height: 20),
                signUpButton,
                SizedBox(height: 15),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void signUp(String email, String password) async {
    if (_formKey.currentState!.validate()) {
      try {
        await _auth
            .createUserWithEmailAndPassword(email: email, password: password)
            .then((value) => {postDetailsToFirestore()})
            .catchError((e) {
          Fluttertoast.showToast(msg: e!.message);
        });
      } on FirebaseAuthException catch (error) {
        switch (error.code) {
          case "invalid-email":
            errorMessage = "Your email address appears to be malformed.";
            break;
          case "wrong-password":
            errorMessage = "Your password is wrong.";
            break;
          case "user-not-found":
            errorMessage = "User with this email doesn't exist.";
            break;
          case "user-disabled":
            errorMessage = "User with this email has been disabled.";
            break;
          case "too-many-requests":
            errorMessage = "Too many requests";
            break;
          case "operation-not-allowed":
            errorMessage = "Signing in with Email and Password is not enabled.";
            break;
          default:
            errorMessage = "An undefined Error happened.";
        }
        Fluttertoast.showToast(msg: errorMessage!);
        print(error.code);
      }
    }
  }

  postDetailsToFirestore() async {
    // calling our firestore
    // calling our user model
    // sedning these values

    FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;
    User? user = _auth.currentUser;

    UserModel userModel = UserModel();

    // writing all the values
    userModel.email = user!.email;
    userModel.uid = user.uid;
    userModel.Nombre = NombreEditingController.text;
    userModel.Apellido = ApellidoEditingController.text;

    await firebaseFirestore
        .collection("users")
        .doc(user.uid)
        .set(userModel.toMap());
    Fluttertoast.showToast(
        msg: "Cuenta creada satisfactoriamente :D ",
        webBgColor: LinearGradient(colors: [
          Color(0xff00406E),
          Color(0xff31AAC1),
        ]));

    Navigator.pushAndRemoveUntil(
        (context),
        MaterialPageRoute(builder: (context) => HomeScreen()),
        (route) => false);
  }
}
