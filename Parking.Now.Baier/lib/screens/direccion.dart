import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class direccion extends StatefulWidget {
  const direccion({Key? key}) : super(key: key);

  @override
  _direccionState createState() => _direccionState();
}

class _direccionState extends State<direccion> {
  TextEditingController direccion = TextEditingController();
  TextEditingController matricula = TextEditingController();
  TextEditingController modelo = TextEditingController();
  TextEditingController telefono = TextEditingController();
  TextEditingController horas = TextEditingController();
  final firebase = FirebaseFirestore.instance;
  create() async {
    try {
      await firebase.collection("direccion").doc(direccion.text).set({
        "direccion": direccion.text,
        "matricula": matricula.text,
        "modelo": modelo.text,
        "telefono": telefono.text,
        "Horas": horas.text,
      });
    } catch (e) {
      print(e);
    }
  }

  update() async {
    try {
      firebase.collection("direccion").doc(direccion.text).update({
        'matricula': matricula.text,
        'modelo': modelo.text,
        'telefono': telefono.text,
        'Horas': horas.text
      });
    } catch (e) {
      print(e);
    }
  }

  delete() async {
    try {
      firebase.collection("direccion").doc(direccion.text).delete();
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff00406E),
        title: Text(
          'PARKING NOW',
          style: TextStyle(
            color: Colors.white,
            fontSize: 25,
            fontWeight: FontWeight.bold,
            fontFamily: 'helios',
            letterSpacing: 5,
          ),
        ),
        titleSpacing: 50,
        elevation: 10,
        centerTitle: true,
      ),
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color(0xff00406E),
                Color(0xff31AAC1),
              ]),
        ),
        child: Container(
          child: Stack(
            children: [
              Container(
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    TextField(
                        style: TextStyle(color: Colors.white.withOpacity(0.80)),
                        autofocus: false,
                        textInputAction: TextInputAction.done,
                        controller: direccion,
                        decoration: InputDecoration(
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                            color: Colors.white.withOpacity(0.80),
                            width: 1,
                          )),
                          prefixIcon: Icon(
                            Icons.add_road,
                            color: Colors.white.withOpacity(0.80),
                          ),
                          labelText: ('Direccion'),
                          labelStyle:
                              TextStyle(color: Colors.white.withOpacity(0.80)),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        )),
                    SizedBox(
                      height: 25,
                    ),
                    TextField(
                        style: TextStyle(color: Colors.white.withOpacity(0.80)),
                        autofocus: false,
                        textInputAction: TextInputAction.done,
                        controller: matricula,
                        decoration: InputDecoration(
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                            color: Colors.white.withOpacity(0.80),
                            width: 1,
                          )),
                          prefixIcon: Icon(
                            Icons.car_rental,
                            color: Colors.white.withOpacity(0.80),
                          ),
                          labelText: ('Matricula'),
                          labelStyle:
                              TextStyle(color: Colors.white.withOpacity(0.80)),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        )),
                    SizedBox(
                      height: 25,
                    ),
                    TextField(
                        style: TextStyle(color: Colors.white.withOpacity(0.80)),
                        autofocus: false,
                        textInputAction: TextInputAction.done,
                        controller: telefono,
                        decoration: InputDecoration(
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                            color: Colors.white.withOpacity(0.80),
                            width: 1,
                          )),
                          prefixIcon: Icon(
                            Icons.phone,
                            color: Colors.white.withOpacity(0.80),
                          ),
                          labelText: ('Telefono'),
                          labelStyle:
                              TextStyle(color: Colors.white.withOpacity(0.80)),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        )),
                    SizedBox(
                      height: 25,
                    ),
                    TextField(
                        style: TextStyle(color: Colors.white.withOpacity(0.80)),
                        autofocus: false,
                        textInputAction: TextInputAction.done,
                        controller: modelo,
                        decoration: InputDecoration(
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                            color: Colors.white.withOpacity(0.80),
                            width: 1,
                          )),
                          prefixIcon: Icon(
                            Icons.car_repair_rounded,
                            color: Colors.white.withOpacity(0.80),
                          ),
                          labelText: ('Modelo'),
                          labelStyle:
                              TextStyle(color: Colors.white.withOpacity(0.80)),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        )),
                    SizedBox(
                      height: 25,
                    ),
                    TextField(
                        style: TextStyle(color: Colors.white.withOpacity(0.80)),
                        autofocus: false,
                        textInputAction: TextInputAction.done,
                        controller: horas,
                        decoration: InputDecoration(
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                            color: Colors.white.withOpacity(0.80),
                            width: 1,
                          )),
                          prefixIcon: Icon(
                            Icons.watch,
                            color: Colors.white.withOpacity(0.80),
                          ),
                          labelText: ('Cantidad de horas'),
                          labelStyle:
                              TextStyle(color: Colors.white.withOpacity(0.80)),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        )),
                    SizedBox(
                      height: 25,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 25,
              ),
              Align(
                alignment: Alignment(0.8, 0.3),
                child: FloatingActionButton(
                  onPressed: () {
                    create();
                    direccion.clear();
                    matricula.clear();
                    modelo.clear();
                    telefono.clear();
                    horas.clear();
                    showDialog(
                        context: context,
                        builder: (context) => AlertDialog(
                              backgroundColor:
                                  Color(0xff00406E).withOpacity(0.75),
                              title: Text(
                                'Creado',
                                style: TextStyle(
                                    color: Colors.white.withOpacity(0.75)),
                              ),
                              content: Text(
                                'Se ha creado la direccion en la base de datos',
                                style: TextStyle(
                                    color: Colors.white.withOpacity(0.75)),
                              ),
                              actions: <Widget>[
                                TextButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: Text(
                                    'Ok',
                                    style: TextStyle(
                                        color: Colors.white.withOpacity(0.75)),
                                  ),
                                )
                              ],
                            ));
                  },
                  tooltip: 'Crear',
                  child: Icon(Icons.check),
                  backgroundColor: Color(0xff00406E).withOpacity(0.75),
                  hoverColor: Colors.white.withOpacity(0.25),
                ),
              ),
              Align(
                alignment: Alignment(0.8, 0.6),
                child: FloatingActionButton(
                  onPressed: () {
                    update();
                    direccion.clear();
                    matricula.clear();
                    modelo.clear();
                    telefono.clear();
                    horas.clear();
                    showDialog(
                        context: context,
                        builder: (context) => AlertDialog(
                              backgroundColor:
                                  Color(0xff00406E).withOpacity(0.75),
                              title: Text(
                                'Modificado',
                                style: TextStyle(
                                    color: Colors.white.withOpacity(0.75)),
                              ),
                              content: Text(
                                'Se ha Modificado la direccion en la base de datos',
                                style: TextStyle(
                                    color: Colors.white.withOpacity(0.75)),
                              ),
                              actions: <Widget>[
                                TextButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: Text(
                                    'Ok',
                                    style: TextStyle(
                                        color: Colors.white.withOpacity(0.75)),
                                  ),
                                )
                              ],
                            ));
                  },
                  tooltip: 'Editar',
                  child: Icon(Icons.edit),
                  backgroundColor: Color(0xff00406E).withOpacity(0.75),
                  hoverColor: Colors.white.withOpacity(0.25),
                ),
              ),
              Align(
                alignment: Alignment(0.8, 0.9),
                child: FloatingActionButton(
                  onPressed: () {
                    delete();
                    direccion.clear();
                    matricula.clear();
                    modelo.clear();
                    telefono.clear();
                    horas.clear();
                    showDialog(
                        context: context,
                        builder: (context) => AlertDialog(
                              backgroundColor:
                                  Color(0xff00406E).withOpacity(0.75),
                              title: Text(
                                'Eliminado',
                                style: TextStyle(
                                    color: Colors.white.withOpacity(0.75)),
                              ),
                              content: Text(
                                'Se ha eliminado la direccion de la base de datos',
                                style: TextStyle(
                                    color: Colors.white.withOpacity(0.75)),
                              ),
                              actions: <Widget>[
                                TextButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: Text(
                                    'Ok',
                                    style: TextStyle(
                                        color: Colors.white.withOpacity(0.75)),
                                  ),
                                )
                              ],
                            ));
                  },
                  tooltip: 'Eliminar',
                  child: Icon(Icons.delete),
                  backgroundColor: Color(0xff00406E).withOpacity(0.75),
                  hoverColor: Colors.white.withOpacity(0.25),
                ),
              ),
              Align(
                alignment: Alignment(-0.8, 0.9),
                child: FloatingActionButton(
                  onPressed: () {
                    Navigator.pushNamed(context, '/UbiScreen');
                  },
                  tooltip: 'Atrás',
                  child: Icon(Icons.arrow_left),
                  backgroundColor: Color(0xff00406E).withOpacity(0.75),
                  hoverColor: Colors.white.withOpacity(0.25),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
