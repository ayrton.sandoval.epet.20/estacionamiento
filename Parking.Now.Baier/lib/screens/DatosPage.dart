import 'package:email_password_login/screens/PagoAceptado.dart';
import 'package:email_password_login/screens/PagoRechazado.dart';
import 'package:email_password_login/screens/home_screen.dart';
import 'package:flutter/material.dart';

/// Este es el widget de la aplicación principal.
class DatosPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(fontFamily: 'Neo'),
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xff00406E),
          title: Text(
            'PARKING NOW',
            style: TextStyle(
              color: Colors.white,
              fontSize: 25,
              fontWeight: FontWeight.bold,
              fontFamily: 'helios',
              letterSpacing: 5,
            ),
          ),
          titleSpacing: 50,
          elevation: 10,
          centerTitle: true,
        ),
        body: Container(
          child: MyStatefulWidget(),
          decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color(0xff31AAC1),
                  Color(0xff00406E),
                ]),
          ),
        ),
      ),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

String dropdownValue = 'Una Hora';

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  get onChanged => null;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(children: [
        Container(
          padding: EdgeInsets.all(50),
          child: Stack(
            children: [
              Align(
                alignment: Alignment(0, -0.5),
                child: TextFormField(
                  style: TextStyle(color: Colors.white),
                  decoration: InputDecoration(
                      border: UnderlineInputBorder(),
                      labelText: "Matricula del Automovil",
                      labelStyle: TextStyle(
                        fontFamily: 'Neo',
                        fontSize: 15,
                        color: Colors.white.withOpacity(0.70),
                      ),
                      icon: Icon(
                        Icons.car_rental,
                        color: Colors.white,
                      ),
                      enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                        color: Colors.white.withOpacity(0.80),
                        width: 1,
                      )),
                      focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                        color: Colors.white,
                        width: 2,
                      ))),
                  cursorColor: Colors.white,
                ),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: DropdownButton<String>(
                  isExpanded: true,
                  dropdownColor: Color(0xff00406E).withOpacity(0.70),
                  value: dropdownValue,
                  iconDisabledColor: Colors.white.withOpacity(0.70),
                  iconEnabledColor: Colors.white,
                  focusColor: Colors.red,
                  style: TextStyle(color: Colors.white.withOpacity(0.70)),
                  onChanged: (String? newValue) {
                    setState(() {
                      dropdownValue = newValue!;
                    });
                  },
                  underline: Container(
                    height: 2,
                    width: 5,
                    color: Colors.white.withOpacity(0.70),
                  ),
                  items: <String>[
                    'Una Hora',
                    'Dos Horas',
                    'Tres Horas',
                    'Cuatro Horas'
                  ].map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(
                        value,
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    );
                  }).toList(),
                ),
              ),
            ],
          ),
        ),
        Align(
          alignment: Alignment(0, 0.5),
          child: SizedBox(
            child: const Text(
              'Total: 500 Dolares ',
              style: TextStyle(color: Colors.white),
              textScaleFactor: 2,
            ),
          ),
        ),
        Align(
          alignment: Alignment(0.8, 0.9),
          child: FloatingActionButton(
            tooltip: 'Confirmar Pago',
            onPressed: () => {},
            child: Icon(Icons.monetization_on),
            backgroundColor: Color(0xff00406E).withOpacity(0.50),
            hoverColor: Colors.white.withOpacity(0.25),
          ),
        ),
        Align(
          alignment: Alignment(-0.8, 0.9),
          child: FloatingActionButton(
            tooltip: 'Atrás',
            onPressed: () => {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => HomeScreen()))
            },
            child: Icon(Icons.arrow_left),
            backgroundColor: Color(0xff00406E).withOpacity(0.50),
            hoverColor: Colors.white.withOpacity(0.25),
          ),
        ),
        Align(
          alignment: Alignment(0.8, -0.9),
          child: FloatingActionButton(
            tooltip: 'Pago aceptado',
            onPressed: () => {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => PagoAceptado()))
            },
            child: Icon(Icons.arrow_right),
            backgroundColor: Color(0xff00406E).withOpacity(0.50),
            hoverColor: Colors.white.withOpacity(0.25),
          ),
        ),
        Align(
          alignment: Alignment(0.8, -0.7),
          child: FloatingActionButton(
            tooltip: 'Pago rechazado',
            onPressed: () => {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => PagoRechazado()))
            },
            child: Icon(Icons.arrow_left),
            backgroundColor: Color(0xff00406E).withOpacity(0.50),
            hoverColor: Colors.white.withOpacity(0.25),
          ),
        ),
      ]),
    );
  }
}
