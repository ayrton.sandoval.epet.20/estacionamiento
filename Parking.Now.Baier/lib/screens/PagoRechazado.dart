import 'package:email_password_login/screens/DatosPage.dart';
import 'package:flutter/material.dart';

class PagoRechazado extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(fontFamily: 'Neo'),
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xff00406E),
          title: Text(
            'PARKING NOW',
            style: TextStyle(
              color: Colors.white,
              fontSize: 25,
              fontWeight: FontWeight.bold,
              fontFamily: 'helios',
              letterSpacing: 5,
            ),
          ),
          titleSpacing: 50,
          elevation: 10,
          centerTitle: true,
        ),
        body: Container(
          child: MyStatefulWidget(),
          decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color(0xff31AAC1),
                  Color(0xff00406E),
                ]),
          ),
        ),
      ),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  get onChanged => null;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(children: [
        Align(
          alignment: Alignment(0, -0.2),
          child: Image.asset(
            '../assets/img/incorrecto.png',
            scale: 0.5,
          ),
        ),
        Align(
          alignment: Alignment(0, 0.2),
          child: Text(
            'Su pago ha sido rechazo lamentablemente',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
            ),
            textScaleFactor: 2,
          ),
        ),
        Align(
          alignment: Alignment(-0.8, 0.9),
          child: FloatingActionButton(
            tooltip: 'Home',
            onPressed: () {
              Navigator.pushNamed(context, '/HomeScreen');
            },
            child: Icon(Icons.home),
            backgroundColor: Color(0xff00406E).withOpacity(0.50),
            hoverColor: Colors.white.withOpacity(0.25),
          ),
        ),
        Align(
          alignment: Alignment(0.8, 0.9),
          child: FloatingActionButton(
            tooltip: 'Inicio',
            onPressed: () {
              Navigator.pushNamed(context, '/HomeScreen');
            },
            child: Icon(Icons.home),
            backgroundColor: Color(0xff00406E).withOpacity(0.50),
            hoverColor: Colors.white.withOpacity(0.25),
          ),
        ),
      ]),
    );
  }
}
