import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:email_password_login/main.dart';
import 'package:email_password_login/model/user_model.dart';
import 'package:email_password_login/screens/Alarma.dart';
import 'package:email_password_login/screens/DatosPage.dart';

import 'package:email_password_login/screens/Ubicaci%C3%B3n.dart';
import 'package:email_password_login/screens/direccion.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geocode/geocode.dart';
import 'package:map/map.dart';
import 'package:latlng/latlng.dart';
import 'login_screen.dart';
import 'DatosPage.dart';
import 'PagoAceptado.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  User? user = FirebaseAuth.instance.currentUser;
  UserModel loggedInUser = UserModel();

  final controller =
      MapController(location: LatLng(-38.95182191695065, -68.05918334618327));

  Offset? _dragStart;
  double _scaleStart = 1.0;
  double lat = -38.95182191695065, long = -68.05918334618327;
  String address = "";
  bool loading = false;
  void _onScaleStart(ScaleStartDetails details) {
    _dragStart = details.focalPoint;
    _scaleStart = 1.0;
  }

  void _onScaleUpdate(ScaleUpdateDetails details) {
    final scaleDiff = details.scale - _scaleStart;
    _scaleStart = details.scale;

    if (scaleDiff > 0)
      controller.zoom += 0.02;
    else if (scaleDiff < 0)
      controller.zoom -= 0.02;
    else {
      final now = details.focalPoint;
      final diff = now - _dragStart!;
      _dragStart = now;
      controller.drag(diff.dx, diff.dy);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff00406E),
        title: Text(
          'PARKING NOW',
          style: TextStyle(
            color: Colors.white,
            fontSize: 25,
            fontWeight: FontWeight.bold,
            fontFamily: 'helios',
            letterSpacing: 5,
          ),
        ),
        titleSpacing: 50,
        elevation: 10,
        centerTitle: true,
      ),
      body: Stack(
        children: [
          Align(
            alignment: Alignment.center,
            child: GestureDetector(
              onScaleStart: _onScaleStart,
              onScaleUpdate: _onScaleUpdate,
              child: Map(
                controller: controller,
                builder: (context, x, y, z) {
                  final url =
                      'https://www.google.com/maps/vt/pb=!1m4!1m3!1i$z!2i$x!3i$y!2m3!1e0!2sm!3i420120488!3m7!2sen!5e1105!12m4!1e68!2m2!1sset!2sRoadmap!4e0!5m1!1e0!23i4111425';
                  return CachedNetworkImage(
                    imageUrl: url,
                    fit: BoxFit.cover,
                  );
                },
              ),
            ),
          ),
          Align(
            alignment: Alignment(0.8, 0.3),
            child: FloatingActionButton(
              onPressed: () {
                Navigator.pushNamed(context, '/UbiScreen');
              },
              tooltip: 'Ubicación',
              child: Icon(Icons.location_on),
              backgroundColor: Color(0xff00406E).withOpacity(0.75),
              hoverColor: Colors.white.withOpacity(0.25),
            ),
          ),
          Align(
            alignment: Alignment(0.8, 0.6),
            child: FloatingActionButton(
              tooltip: 'Alarma',
              onPressed: () {
                Navigator.pushNamed(context, '/alarma');
              },
              child: Icon(Icons.alarm),
              backgroundColor: Color(0xff00406E).withOpacity(0.75),
              hoverColor: Colors.white.withOpacity(0.25),
            ),
          ),
          Align(
            alignment: Alignment(0.8, 0.9),
            child: FloatingActionButton(
              tooltip: 'Aceptar Pago',
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (context) => AlertDialog(
                          backgroundColor: Color(0xff00406E).withOpacity(0.75),
                          title: Text(
                            'Pago Virtual',
                            style: TextStyle(
                                color: Colors.white.withOpacity(0.75)),
                          ),
                          content: Text(
                            'Se ha aceptado su pago correctamente',
                            style: TextStyle(
                                color: Colors.white.withOpacity(0.75)),
                          ),
                          actions: <Widget>[
                            TextButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text(
                                'Ok',
                                style: TextStyle(
                                    color: Colors.white.withOpacity(0.75)),
                              ),
                            )
                          ],
                        ));
              },
              child: Icon(Icons.payment),
              backgroundColor: Color(0xff00406E).withOpacity(0.75),
              hoverColor: Colors.white.withOpacity(0.25),
            ),
          ),
        ],
      ),
    );
  }

  // the logout function
  Future<void> logout(BuildContext context) async {
    await FirebaseAuth.instance.signOut();
    Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => LoginScreen()));
  }
}
