import 'dart:html';
import 'package:flutter/material.dart';
import 'package:parking_now/Paginas/LoginPage.dart';
import 'package:parking_now/Paginas/RegistrarsePage.dart';
import 'package:parking_now/Paginas/RegistrarsePage.dart';
import 'Paginas/HomePage.dart';

void main() => runApp(Mein());

class Mein extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(fontFamily: 'Neo'),
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xff00406E),
          title: Text(
            'PARKING NOW',
            style: TextStyle(
              color: Colors.white,
              fontSize: 25,
              fontWeight: FontWeight.bold,
              fontFamily: 'helios',
              letterSpacing: 5,
            ),
          ),
          titleSpacing: 50,
          elevation: 10,
          centerTitle: true,
        ),
        body: Container(
          child: MyStatefulWidget(),
          decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color(0xff31AAC1),
                  Color(0xff00406E),
                ]),
          ),
        ),
      ),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            child: Image.asset(
              '../assets/img/logo.png',
              scale: 2,
            ),
          ),
          SizedBox(
            height: 150,
          ),
          ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                minimumSize: Size(250, 50),
                primary: Colors.white.withOpacity(0.50),
                onPrimary: Colors.white,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50)),
              ),
              onPressed: () => {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => LoginPage()))
                  },
              icon: Icon(Icons.person),
              label: Text('Iniciar Sesion',
                  style: TextStyle(
                    color: Colors.white.withOpacity(0.80),
                  ))),
        ],
      ),
    );
  }
}
