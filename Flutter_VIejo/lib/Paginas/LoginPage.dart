import 'dart:html';
import 'package:flutter/material.dart';
import 'package:parking_now/Paginas/HomePage.dart';
import 'package:parking_now/Paginas/RegistrarsePage.dart';
import 'package:parking_now/Paginas/RegistrarsePage.dart';

void main() => runApp(LoginPage());

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(fontFamily: 'Neo'),
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xff00406E),
          title: Text(
            'PARKING NOW',
            style: TextStyle(
              color: Colors.white,
              fontSize: 25,
              fontWeight: FontWeight.bold,
              fontFamily: 'helios',
              letterSpacing: 5,
            ),
          ),
          titleSpacing: 50,
          elevation: 10,
          centerTitle: true,
        ),
        body: Container(
          child: MyStatefulWidget(),
          decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color(0xff00406E),
                  Color(0xff31AAC1),
                ]),
          ),
        ),
      ),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            child: Image.asset(
              '../assets/img/logo.png',
              scale: 4,
            ),
          ),
          SizedBox(
            height: 25,
          ),
          Container(
            padding: EdgeInsets.all(25),
            child: TextFormField(
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: "Gmail",
                  labelStyle: TextStyle(
                    fontSize: 15,
                    color: Colors.white.withOpacity(0.70),
                  ),
                  icon: Icon(
                    Icons.email,
                    color: Colors.white,
                  ),
                  enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                    color: Colors.white.withOpacity(0.80),
                    width: 1,
                  )),
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                    color: Colors.white,
                    width: 2,
                  ))),
              keyboardType: TextInputType.emailAddress,
              cursorColor: Colors.white,
            ),
          ),
          Container(
            padding: EdgeInsets.all(25),
            child: TextFormField(
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: "Contraseña",
                  labelStyle: TextStyle(
                    fontSize: 15,
                    color: Colors.white.withOpacity(0.70),
                  ),
                  icon: Icon(
                    Icons.security,
                    color: Colors.white,
                  ),
                  enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                    color: Colors.white.withOpacity(0.80),
                    width: 1,
                  )),
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                    color: Colors.white,
                    width: 2,
                  ))),
              keyboardType: TextInputType.visiblePassword,
              obscureText: true,
              cursorColor: Colors.white,
            ),
          ),
          SizedBox(
            height: 50,
          ),
          ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                minimumSize: Size(250, 50),
                primary: Colors.white.withOpacity(0.50),
                onPrimary: Colors.white,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50)),
              ),
              onPressed: () => {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => HomePage()))
                  },
              icon: Icon(Icons.person),
              label: Text('Iniciar Sesion',
                  style: TextStyle(
                    color: Colors.white.withOpacity(0.80),
                  ))),
          SizedBox(
            height: 25,
          ),
          ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                minimumSize: Size(250, 50),
                primary: Colors.white.withOpacity(0.50),
                onPrimary: Colors.white,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50)),
              ),
              onPressed: () => {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => RegistrarsePage()))
                  },
              icon: Icon(Icons.person),
              label: Text('Registrarse',
                  style: TextStyle(
                    color: Colors.white.withOpacity(0.80),
                  ))),
        ],
      ),
    );
  }
}
