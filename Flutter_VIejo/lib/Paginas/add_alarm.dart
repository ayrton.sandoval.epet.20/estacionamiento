import 'package:flutter/material.dart';
import 'package:parking_now/Paginas/Alarma.dart';
import 'package:parking_now/widgets/circle_day.dart';

class AddAlarm extends StatefulWidget {
  _AddAlarmState createState() => _AddAlarmState();
}

class _AddAlarmState extends State<AddAlarm> {
  late TimeOfDay _selectedTime;
  late ValueChanged<TimeOfDay> selectTime;

  @override
  void initState() {
    _selectedTime = new TimeOfDay(hour: 12, minute: 30);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff00406E),
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Color(0xff00406E),
        title: Column(
          children: <Widget>[
            Icon(
              Icons.alarm_add,
              color: Color(0xff00406E),
            ),
            Text('Añadir Alarma',
                style: TextStyle(color: Colors.white, fontSize: 25.0)),
            SizedBox(
              height: 25,
            )
          ],
        ),
      ),
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color(0xff00406E),
                Color(0xff31AAC1),
              ]),
        ),
        child: Padding(
            padding: EdgeInsets.all(20.0),
            child: Container(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 60.0,
                  ),
                  new GestureDetector(
                    child: Text(
                      _selectedTime.format(context),
                      style: TextStyle(
                        fontSize: 60.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                    onTap: () {
                      _selectTime(context);
                    },
                  ),
                  SizedBox(
                    height: 30.0,
                  ),
                  SizedBox(
                    height: 2.0,
                    child: Container(
                      color: Colors.white30,
                    ),
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.check_box,
                      color: Colors.white,
                    ),
                    title: Text('Notificación',
                        style: TextStyle(color: Colors.white)),
                  ),
                  SizedBox(
                    height: 2.0,
                    child: Container(
                      color: Colors.white30,
                    ),
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.check_box,
                      color: Colors.white,
                    ),
                    title:
                        Text('Vibrar', style: TextStyle(color: Colors.white)),
                  ),
                  SizedBox(
                    height: 2.0,
                    child: Container(
                      color: Colors.white30,
                    ),
                  ),
                  SizedBox(
                    height: 270.0,
                  ),
                  Stack(
                    children: [
                      Align(
                        alignment: Alignment(0.8, 0.9),
                        child: FloatingActionButton(
                          onPressed: () => Navigator.of(context).pop(),
                          child: Icon(Icons.check),
                          backgroundColor: Color(0xff00406E).withOpacity(0.50),
                          hoverColor: Colors.white.withOpacity(0.25),
                        ),
                      ),
                      Align(
                        alignment: Alignment(-0.8, 0.9),
                        child: FloatingActionButton(
                          onPressed: () => Navigator.of(context).pop(),
                          child: Icon(Icons.delete),
                          backgroundColor: Color(0xff00406E).withOpacity(0.50),
                          hoverColor: Colors.white.withOpacity(0.25),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            )),
      ),
    );
  }

  Future<void> _selectTime(BuildContext context) async {
    final TimeOfDay? picked =
        await showTimePicker(context: context, initialTime: _selectedTime);
    setState(() {
      _selectedTime = picked!;
    });
  }
}
