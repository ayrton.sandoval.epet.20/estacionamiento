import 'package:flutter/material.dart';
import 'package:parking_now/Paginas/Datos_Auto.dart';
import 'package:parking_now/Paginas/HomePage.dart';
import 'package:parking_now/main.dart';

void main() => runApp(const Pr());

/// Este es el widget de la aplicación principal.
class Pr extends StatelessWidget {
  const Pr({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(fontFamily: 'Neo'),
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xff00406E),
          title: Text(
            'PARKING NOW',
            style: TextStyle(
              color: Colors.white,
              fontSize: 25,
              fontWeight: FontWeight.bold,
              fontFamily: 'helios',
              letterSpacing: 5,
            ),
          ),
          titleSpacing: 50,
          elevation: 10,
          centerTitle: true,
        ),
        body: Container(
          child: const MyStatefulWidget(),
          decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color(0xff31AAC1),
                  Color(0xff00406E),
                ]),
          ),
        ),
        drawer: Drawer(
          child: Container(
            color: Color(0xff00406E),
            child: ListView(
              padding: EdgeInsets.zero,
              children: <Widget>[
                UserAccountsDrawerHeader(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Color(0xff31AAC1),
                          Color(0xff00406E),
                        ]),
                  ),
                  accountName: Text('Juan Ayrton Sandoval'),
                  accountEmail: Text('Juanayrtonsandoval@gmail.com'),
                  currentAccountPicture: CircleAvatar(
                    backgroundColor: Colors.white,
                    child: Text(
                      'A',
                      style: TextStyle(fontSize: 50, fontFamily: 'Neo'),
                    ),
                  ),
                ),
                ListTile(
                  title: Text(
                    'Reportar un Error',
                    style: TextStyle(color: Colors.white),
                  ),
                  leading: Icon(
                    Icons.error,
                    color: Colors.white,
                  ),
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
                ListTile(
                  title: const Text(
                    'Cerrar Sesion',
                    style: TextStyle(color: Colors.white),
                  ),
                  leading: Icon(
                    Icons.outbond,
                    color: Colors.white,
                  ),
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

/// Este es el widget con estado que crea una instancia de la aplicación principal.
class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

/// Esta es la clase de estado privada que va con MyStatefulWidget.
class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  get onChanged => null;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(children: [
        Align(
          alignment: Alignment(0, -0.2),
          child: Image.asset(
            '../assets/img/incorrecto.png',
            scale: 0.5,
          ),
        ),
        Align(
          alignment: Alignment(0, 0.2),
          child: Text(
            'Su pago ha sido rechazo lamentablemente',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
            ),
            textScaleFactor: 2,
          ),
        ),
        Align(
          alignment: Alignment(-0.8, 0.9),
          child: FloatingActionButton(
            tooltip: 'Atrás',
            onPressed: () => {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Aut()))
            },
            child: Icon(Icons.car_rental),
            backgroundColor: Color(0xff00406E).withOpacity(0.50),
            hoverColor: Colors.white.withOpacity(0.25),
          ),
        ),
        Align(
          alignment: Alignment(0.8, 0.9),
          child: FloatingActionButton(
            tooltip: 'Inicio',
            onPressed: () => {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => HomePage()))
            },
            child: Icon(Icons.home),
            backgroundColor: Color(0xff00406E).withOpacity(0.50),
            hoverColor: Colors.white.withOpacity(0.25),
          ),
        ),
      ]),
      //padding: EdgeInsets.all(50),
    );
  }
}
