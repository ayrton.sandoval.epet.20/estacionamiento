import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:parking_now/Paginas/Alarma.dart';
import 'package:parking_now/Paginas/DatosPage.dart';
import 'package:parking_now/Paginas/LoginPage.dart';
import 'package:parking_now/Paginas/RegistrarsePage.dart';
import 'package:parking_now/Paginas/Ubicaci%C3%B3n.dart';
import 'package:parking_now/main.dart';
import '../main.dart';

void main() => runApp(HomePage());

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(fontFamily: 'Neo'),
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xff00406E),
          title: Text(
            'PARKING NOW',
            style: TextStyle(
              color: Colors.white,
              fontSize: 25,
              fontWeight: FontWeight.bold,
              fontFamily: 'helios',
              letterSpacing: 5,
            ),
          ),
          titleSpacing: 50,
          elevation: 10,
          centerTitle: true,
        ),
        body: Container(
          child: const MyStatefulWidget(),
          decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color(0xff31AAC1),
                  Color(0xff00406E),
                ]),
          ),
        ),
        drawer: Drawer(
          child: Container(
            color: Color(0xff00406E),
            child: ListView(
              padding: EdgeInsets.zero,
              children: <Widget>[
                UserAccountsDrawerHeader(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Color(0xff31AAC1),
                          Color(0xff00406E),
                        ]),
                  ),
                  accountName: Text('Ayrton Sandoval'),
                  accountEmail: Text('Juanayrtonsandoval@gmail.com'),
                  currentAccountPicture: CircleAvatar(
                    backgroundColor: Colors.white,
                    child: Text(
                      'A',
                      style: TextStyle(fontSize: 50, fontFamily: 'Neo'),
                    ),
                  ),
                ),
                ListTile(
                  title: Text(
                    'Reportar un Error',
                    style: TextStyle(color: Colors.white),
                  ),
                  leading: Icon(
                    Icons.error,
                    color: Colors.white,
                  ),
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
                ListTile(
                  title: const Text(
                    'Cerrar Sesion',
                    style: TextStyle(color: Colors.white),
                  ),
                  leading: Icon(
                    Icons.outbond,
                    color: Colors.white,
                  ),
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => LoginPage()));
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

/// Este es el widget con estado que crea una instancia de la aplicación principal.
class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

/// Esta es la clase de estado privada que va con MyStatefulWidget.
class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  @override
  Widget build(BuildContext context) {
    final ButtonStyle style =
        ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 100));

    return Container(
      child: Stack(
        children: [
          Align(
            alignment: Alignment.center,
            child: Image.asset(
              '../assets/img/ubi.jpeg',
              scale: 0.01,
            ),
          ),
          Align(
            alignment: Alignment(0.8, 0.3),
            child: FloatingActionButton(
              tooltip: 'Selecciona la ubicación',
              onPressed: () => {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => UbicacionPage()))
              },
              child: Icon(Icons.person_pin_circle),
              backgroundColor: Color(0xff00406E).withOpacity(0.50),
              hoverColor: Colors.white.withOpacity(0.25),
            ),
          ),
          Align(
            alignment: Alignment(0.8, 0.6),
            child: FloatingActionButton(
              tooltip: 'Alarma',
              onPressed: () => {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => alarma()))
              },
              child: Icon(Icons.alarm),
              backgroundColor: Color(0xff00406E).withOpacity(0.50),
              hoverColor: Colors.white.withOpacity(0.25),
            ),
          ),
          Align(
            alignment: Alignment(0.8, 0.9),
            child: FloatingActionButton(
              tooltip: 'Pagar',
              onPressed: () => {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => DatosPage()))
              },
              child: Icon(Icons.monetization_on),
              backgroundColor: Color(0xff00406E).withOpacity(0.50),
              hoverColor: Colors.white.withOpacity(0.25),
            ),
          ),
        ],
      ),
    );
  }
}
