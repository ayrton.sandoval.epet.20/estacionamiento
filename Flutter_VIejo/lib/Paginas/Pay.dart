import 'package:flutter/material.dart';
import 'package:parking_now/Paginas/Datos_Auto.dart';

import 'package:parking_now/Paginas/Pay.dart';
import 'package:parking_now/Paginas/ubicacion.dart';
import 'package:parking_now/main.dart';
import '../main.dart';

void main() => runApp(const Pag());

/// Este es el widget de la aplicación principal.
class Pag extends StatelessWidget {
  const Pag({Key? key}) : super(key: key);

  static const String _title = 'Pagar';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      home: Scaffold(
        appBar: AppBar(title: const Text(_title)),
        body: Container(
          child: const MyStatefulWidget(),
          decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color(0xff00406E),
                  Color(0xff31AAC1),
                ]),
          ),
        ),
        drawer: Drawer(
          // Add a ListView to the drawer. This ensures the user can scroll
          // through the options in the drawer if there isn't enough vertical
          // space to fit everything.
          child: ListView(
            // Important: Remove any padding from the ListView.
            padding: EdgeInsets.zero,
            children: <Widget>[
              UserAccountsDrawerHeader(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        Colors.blueAccent.shade400,
                        Colors.lightBlue.shade100,
                      ]),
                ),
                accountName: Text('Juan Ayrton Sandoval'),
                accountEmail: Text('Juanayrtonsandoval@gmail.com'),
                currentAccountPicture: CircleAvatar(
                  backgroundColor: Colors.blueAccent[100],
                  child: Text(
                    'A',
                    style: TextStyle(fontSize: 50),
                  ),
                ),
              ),
              ListTile(
                title: const Text(
                  'Añadir Tarjeta',
                  //style: TextStyle(fontSize: 20),
                ),
                leading: Icon(Icons.card_giftcard),
                //tileColor: Colors.blueAccent,
                onTap: () {
                  // Update the state of the app
                  // ...
                  // Then close the drawer
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title: const Text('Reportar un Error'),
                leading: Icon(Icons.error),
                onTap: () {
                  // Update the state of the app
                  // ...
                  // Then close the drawer
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title: const Text('Cerrar Sesion'),
                leading: Icon(Icons.outbond),
                onTap: () {
                  // Update the state of the app
                  // ...
                  // Then close the drawer
                  Navigator.pop(context);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

/// Este es el widget con estado que crea una instancia de la aplicación principal.
class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

/// Esta es la clase de estado privada que va con MyStatefulWidget.
class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(25),
      child: Form(
        child: Column(
          children: <Widget>[
            TextFormField(
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: "Número de tarjeta",
                  labelStyle: TextStyle(
                    fontSize: 15,
                    color: Colors.white.withOpacity(0.70),
                  ),
                  icon: Icon(
                    Icons.card_giftcard,
                    color: Colors.white,
                  ),
                  enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                    color: Colors.white.withOpacity(0.80),
                    width: 1,
                  )),
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                    color: Colors.white,
                    width: 2,
                  ))),
              cursorColor: Colors.white,
              keyboardType: TextInputType.phone,
            ),
            SizedBox(
              height: 25,
            ),
            TextFormField(
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: "Nombre y Apellido",
                  labelStyle: TextStyle(
                    fontSize: 15,
                    color: Colors.white.withOpacity(0.70),
                  ),
                  icon: Icon(
                    Icons.person_add,
                    color: Colors.white,
                  ),
                  enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                    color: Colors.white.withOpacity(0.80),
                    width: 1,
                  )),
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                    color: Colors.white,
                    width: 2,
                  ))),
              cursorColor: Colors.white,
            ),
            SizedBox(
              height: 25,
            ),
            TextFormField(
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: "Codigo de Seguridad",
                  labelStyle: TextStyle(
                    fontSize: 15,
                    color: Colors.white.withOpacity(0.70),
                  ),
                  icon: Icon(
                    Icons.security,
                    color: Colors.white,
                  ),
                  enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                    color: Colors.white.withOpacity(0.80),
                    width: 1,
                  )),
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                    color: Colors.white,
                    width: 2,
                  ))),
              obscureText: true,
              maxLength: 3,
              cursorColor: Colors.white,
            ),
            SizedBox(
              height: 15,
            ),
            TextFormField(
              style: TextStyle(color: Colors.white),
              decoration: InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: "Fecha de vencimiento",
                  labelStyle: TextStyle(
                    fontSize: 15,
                    color: Colors.white.withOpacity(0.70),
                  ),
                  icon: Icon(
                    Icons.date_range,
                    color: Colors.white,
                  ),
                  enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                    color: Colors.white.withOpacity(0.80),
                    width: 1,
                  )),
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                    color: Colors.white,
                    width: 2,
                  ))),
              keyboardType: TextInputType.datetime,
              cursorColor: Colors.white,
            ),
            SizedBox(
              height: 50,
            ),
            Stack(
              children: [
                Align(
                  alignment: Alignment.bottomRight,
                  child: ElevatedButton.icon(
                      style: ElevatedButton.styleFrom(
                        minimumSize: Size(50, 50),
                        primary: Colors.white.withOpacity(0.50),
                        onPrimary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50)),
                      ),
                      onPressed: () => {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) => Aut()))
                          },
                      icon: Icon(Icons.arrow_right),
                      label: Text(
                        'Siguiente',
                      )),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: ElevatedButton.icon(
                      style: ElevatedButton.styleFrom(
                        minimumSize: Size(50, 50),
                        primary: Colors.white.withOpacity(0.50),
                        onPrimary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50)),
                      ),
                      onPressed: () {},
                      icon: Icon(Icons.card_travel),
                      label: Text(
                        'Seleccionar Tarjeta',
                      )),
                ),
                Align(
                  alignment: Alignment.bottomLeft,
                  child: ElevatedButton.icon(
                      style: ElevatedButton.styleFrom(
                        minimumSize: Size(50, 50),
                        primary: Colors.white.withOpacity(0.50),
                        onPrimary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50)),
                      ),
                      onPressed: () => {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) => P2()))
                          },
                      icon: Icon(Icons.arrow_left),
                      label: Text(
                        'Atrás',
                      )),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
