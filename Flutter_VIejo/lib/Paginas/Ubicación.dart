import 'dart:html';

import 'package:flutter/material.dart';
import 'package:parking_now/Paginas/HomePage.dart';

import 'LoginPage.dart';

void main() => runApp(UbicacionPage());

class UbicacionPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(fontFamily: 'Neo'),
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xff00406E),
          title: Text(
            'PARKING NOW',
            style: TextStyle(
              color: Colors.white,
              fontSize: 25,
              fontWeight: FontWeight.bold,
              fontFamily: 'helios',
              letterSpacing: 5,
            ),
          ),
          titleSpacing: 50,
          elevation: 10,
          centerTitle: true,
        ),
        body: Container(
          child: MyStatefulWidget(),
          decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color(0xff31AAC1),
                  Color(0xff00406E),
                ]),
          ),
        ),
        drawer: Drawer(
          child: Container(
            color: Color(0xff00406E),
            child: ListView(
              padding: EdgeInsets.zero,
              children: <Widget>[
                UserAccountsDrawerHeader(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Color(0xff31AAC1),
                          Color(0xff00406E),
                        ]),
                  ),
                  accountName: Text('Ayrton Sandoval'),
                  accountEmail: Text('Juanayrtonsandoval@gmail.com'),
                  currentAccountPicture: CircleAvatar(
                    backgroundColor: Colors.white,
                    child: Text(
                      'A',
                      style: TextStyle(fontSize: 50, fontFamily: 'Neo'),
                    ),
                  ),
                ),
                ListTile(
                  title: Text(
                    'Reportar un Error',
                    style: TextStyle(color: Colors.white),
                  ),
                  leading: Icon(
                    Icons.error,
                    color: Colors.white,
                  ),
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
                ListTile(
                  title: const Text(
                    'Cerrar Sesion',
                    style: TextStyle(color: Colors.white),
                  ),
                  leading: Icon(
                    Icons.outbond,
                    color: Colors.white,
                  ),
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => LoginPage()));
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          Align(
            alignment: Alignment.center,
            child: Image.asset(
              '../assets/img/ubi.jpeg',
              scale: 0.01,
            ),
          ),
          Align(
            alignment: Alignment(0.8, 0.6),
            child: FloatingActionButton(
              onPressed: () {},
              tooltip: 'Selecciona la ubicación',
              child: Icon(Icons.location_on),
              backgroundColor: Color(0xff00406E).withOpacity(0.50),
              hoverColor: Colors.white.withOpacity(0.25),
            ),
          ),
          Align(
            alignment: Alignment(0.8, 0.9),
            child: FloatingActionButton(
              tooltip: 'Atrás',
              onPressed: () => {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => HomePage()))
              },
              child: Icon(Icons.arrow_left),
              backgroundColor: Color(0xff00406E).withOpacity(0.50),
              hoverColor: Colors.white.withOpacity(0.25),
            ),
          ),
        ],
      ),
    );
  }
}
