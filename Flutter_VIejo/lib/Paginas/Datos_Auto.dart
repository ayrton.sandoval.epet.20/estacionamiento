import 'package:flutter/material.dart';
import 'package:parking_now/Paginas/Pa.dart';
import 'package:parking_now/Paginas/RegistrarsePage.dart';
import 'package:parking_now/Paginas/Pr.dart';
import 'package:parking_now/Paginas/HomePage.dart';
import 'package:parking_now/main.dart';

import 'LoginPage.dart';

void main() => runApp(const Aut());

/// Este es el widget de la aplicación principal.
class Aut extends StatelessWidget {
  const Aut({Key? key}) : super(key: key);

  static const String _title = 'Datos del vehiculo';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(fontFamily: 'Neo'),
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xff00406E),
          title: Text(
            'PARKING NOW',
            style: TextStyle(
              color: Colors.white,
              fontSize: 25,
              fontWeight: FontWeight.bold,
              fontFamily: 'helios',
              letterSpacing: 5,
            ),
          ),
          titleSpacing: 50,
          elevation: 10,
          centerTitle: true,
        ),
        body: Container(
          child: const MyStatefulWidget(),
          decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color(0xff00406E),
                  Color(0xff31AAC1),
                ]),
          ),
        ),
        drawer: Drawer(
          child: Container(
            color: Color(0xff00406E),
            child: ListView(
              padding: EdgeInsets.zero,
              children: <Widget>[
                UserAccountsDrawerHeader(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Color(0xff31AAC1),
                          Color(0xff00406E),
                        ]),
                  ),
                  accountName: Text('Ayrton Sandoval'),
                  accountEmail: Text('Juanayrtonsandoval@gmail.com'),
                  currentAccountPicture: CircleAvatar(
                    backgroundColor: Colors.white,
                    child: Text(
                      'A',
                      style: TextStyle(fontSize: 50, fontFamily: 'Neo'),
                    ),
                  ),
                ),
                ListTile(
                  title: Text(
                    'Reportar un Error',
                    style: TextStyle(color: Colors.white),
                  ),
                  leading: Icon(
                    Icons.error,
                    color: Colors.white,
                  ),
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
                ListTile(
                  title: const Text(
                    'Cerrar Sesion',
                    style: TextStyle(color: Colors.white),
                  ),
                  leading: Icon(
                    Icons.outbond,
                    color: Colors.white,
                  ),
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => LoginPage()));
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

/// Este es el widget con estado que crea una instancia de la aplicación principal.
class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

String dropdownValue = 'Una Hora';

/// Esta es la clase de estado privada que va con MyStatefulWidget.
class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  get onChanged => null;

  @override
  Widget build(BuildContext context) {
    return Container(
      //padding: EdgeInsets.all(50),
      child: Stack(children: [
        Container(
          padding: EdgeInsets.all(50),
          child: Stack(
            children: [
              Align(
                alignment: Alignment(0, -0.5),
                child: TextFormField(
                  style: TextStyle(color: Colors.white),
                  decoration: InputDecoration(
                      border: UnderlineInputBorder(),
                      labelText: "Matricula del Automovil",
                      labelStyle: TextStyle(
                        fontFamily: 'Neo',
                        fontSize: 15,
                        color: Colors.white.withOpacity(0.70),
                      ),
                      icon: Icon(
                        Icons.car_rental,
                        color: Colors.white,
                      ),
                      enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                        color: Colors.white.withOpacity(0.80),
                        width: 1,
                      )),
                      focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                        color: Colors.white,
                        width: 2,
                      ))),
                  cursorColor: Colors.white,
                ),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: DropdownButton<String>(
                  isExpanded: true,

                  dropdownColor: Color(0xff00406E).withOpacity(0.70),
                  value: dropdownValue,

                  iconDisabledColor: Colors.white.withOpacity(0.70),
                  iconEnabledColor: Colors.white,
                  focusColor: Colors.red,

                  style: TextStyle(color: Colors.white.withOpacity(0.70)),
                  onChanged: (String? newValue) {
                    setState(() {
                      dropdownValue = newValue!;
                    });
                  },
                  underline: Container(
                    height: 2,
                    width: 5,
                    color: Colors.white.withOpacity(0.70),
                  ),

                  items: <String>[
                    'Una Hora',
                    'Dos Horas',
                    'Tres Horas',
                    'Cuatro Horas'
                  ].map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(
                        value,
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    );
                  }).toList(),
                  //focusColor: Colors.white,
                ),
              ),
            ],
          ),
        ),
        Align(
          alignment: Alignment(0, 0.5),
          child: SizedBox(
            child: const Text(
              'Total: 500 Dolares ',
              style: TextStyle(color: Colors.white),
              textScaleFactor: 2,
            ),
          ),
        ),
        Align(
          alignment: Alignment(0.8, 0.7),
          child: FloatingActionButton(
            tooltip: 'Confirmar Pago',
            onPressed: () => {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Aut()))
            },
            child: Icon(Icons.monetization_on),
            backgroundColor: Color(0xff00406E).withOpacity(0.50),
            hoverColor: Colors.white.withOpacity(0.25),
          ),
        ),
        Align(
          alignment: Alignment(0.8, 0.9),
          child: FloatingActionButton(
            tooltip: 'Atrás',
            onPressed: () => {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => HomePage()))
            },
            child: Icon(Icons.arrow_left),
            backgroundColor: Color(0xff00406E).withOpacity(0.50),
            hoverColor: Colors.white.withOpacity(0.25),
          ),
        ),
        Align(
          alignment: Alignment(0.8, -0.8),
          child: FloatingActionButton(
            tooltip: 'Pago aceptado',
            onPressed: () => {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Pa()))
            },
            child: Icon(Icons.arrow_left),
            backgroundColor: Color(0xff00406E).withOpacity(0.50),
            hoverColor: Colors.white.withOpacity(0.25),
          ),
        ),
        Align(
          alignment: Alignment(0.8, -0.9),
          child: FloatingActionButton(
            tooltip: 'Pago rechazado',
            onPressed: () => {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Pr()))
            },
            child: Icon(Icons.arrow_right),
            backgroundColor: Color(0xff00406E).withOpacity(0.50),
            hoverColor: Colors.white.withOpacity(0.25),
          ),
        ),
      ]),
      //padding: EdgeInsets.all(50),
    );
  }
}
