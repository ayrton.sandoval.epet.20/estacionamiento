import 'package:flutter/material.dart';
import 'package:parking_now/Paginas/Alarma.dart';
import 'package:parking_now/Paginas/Datos_Auto.dart';
import 'package:parking_now/Paginas/Pay.dart';
import 'package:parking_now/Paginas/mapa.dart';
import 'package:parking_now/main.dart';
import '../main.dart';

void main() => runApp(const P3());

class P3 extends StatelessWidget {
  const P3({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(fontFamily: 'Neo'),
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xff00406E),
          title: Text(
            'PARKING NOW',
            style: TextStyle(
              color: Colors.white,
              fontSize: 25,
              fontWeight: FontWeight.bold,
              fontFamily: 'helios',
              letterSpacing: 5,
            ),
          ),
          titleSpacing: 50,
          elevation: 10,
          centerTitle: true,
          leading: IconButton(
            onPressed: () => {},
            icon: Icon(Icons.settings),
            color: Colors.white,
          ),
        ),
        body: Container(
          child: const MyStatefulWidget(),
          decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color(0xff31AAC1),
                  Color(0xff00406E),
                ]),
          ),
        ),
      ),
    );
  }
}

/// Este es el widget con estado que crea una instancia de la aplicación principal.
class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

/// Esta es la clase de estado privada que va con MyStatefulWidget.
class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  @override
  Widget build(BuildContext context) {
    final ButtonStyle style =
        ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 100));

    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            child: Image.asset(
              '../assets/img/ubi.jpeg',
              scale: 2,
            ),
          ),
          SizedBox(height: 25),
          ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                minimumSize: Size(250, 50),
                primary: Colors.white.withOpacity(0.50),
                onPrimary: Colors.white,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50)),
              ),
              onPressed: () => {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Mapa()))
                  },
              icon: Icon(Icons.person_pin_circle),
              label: Text('Elegir Ubicación',
                  style: TextStyle(
                    color: Colors.white.withOpacity(0.80),
                  ))),
          SizedBox(height: 25),
          ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                minimumSize: Size(250, 50),
                primary: Colors.white.withOpacity(0.50),
                onPrimary: Colors.white,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50)),
              ),
              onPressed: () => {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => alarma()))
                  },
              icon: Icon(Icons.alarm),
              label: Text('Alarma',
                  style: TextStyle(
                    color: Colors.white.withOpacity(0.80),
                  ))),
          const SizedBox(height: 25),
          ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                minimumSize: Size(250, 50),
                primary: Colors.white.withOpacity(0.50),
                onPrimary: Colors.white,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50)),
              ),
              onPressed: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => Aut()));
              },
              icon: Icon(Icons.monetization_on),
              label: Text('Pagar',
                  style: TextStyle(
                    color: Colors.white.withOpacity(0.80),
                  ))),
          const SizedBox(
            height: 50,
          ),
          ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                minimumSize: Size(50, 50),
                primary: Colors.white.withOpacity(0.50),
                onPrimary: Colors.white,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50)),
              ),
              onPressed: () => {
                    Navigator.push(
                        context, MaterialPageRoute(builder: (context) => P1()))
                  },
              icon: Icon(Icons.arrow_left),
              label: Text(
                'Atrás',
              )),
        ],
      ),
    );
  }
}
