import 'dart:async';
import 'dart:html';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:parking_now/Paginas/HomePage.dart';
import 'package:parking_now/Paginas/add_alarm.dart';
import 'package:parking_now/main.dart';
import 'package:parking_now/widgets/alarm_item.dart';

import '../shapes_painter.dart';

class alarma extends StatefulWidget {
  alarma({Key? key}) : super(key: key);

  _alarmaState createState() => _alarmaState();
}

class _alarmaState extends State<alarma> with SingleTickerProviderStateMixin {
  late String _timeString;
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this, initialIndex: 0);
    _tabController.addListener(_handleTabIndex);

    _timeString = _formatDateTime(DateTime.now());
    Timer.periodic(Duration(seconds: 1), (Timer t) => _getTime());
  }

  @override
  void dispose() {
    _tabController.removeListener(_handleTabIndex);
    _tabController.dispose();
    super.dispose();
  }

  void _handleTabIndex() {
    setState(() {});
  }

  void _getTime() {
    final DateTime now = DateTime.now();
    final String formattedDateTime = _formatDateTime(now);
    setState(() {
      _timeString = formattedDateTime;
    });
  }

  String _formatDateTime(DateTime dateTime) {
    return DateFormat('hh:mm').format(dateTime);
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(83),
          child: Container(
            padding: EdgeInsets.only(top: 30),
            color: Color(0xff00406E),
            child: TabBar(
              controller: _tabController,
              indicatorColor: Colors.white,
              indicatorWeight: 2.0,
              tabs: [
                Tab(
                  icon: Icon(Icons.access_time),
                  text: 'Hora',
                ),
                Tab(icon: Icon(Icons.alarm), text: 'Alarma'),
              ],
            ),
          ),
        ),
        body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color(0xff00406E),
                  Color(0xff31AAC1),
                ]),
          ),
          child: TabBarView(
            controller: _tabController,
            children: [
              Container(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 25,
                    ),
                    Text(
                      _timeString.toString(),
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 25,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(2.0),
                      child: CustomPaint(
                        painter: ShapesPainter(),
                        child: Container(
                          height: 350,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 100,
                    ),
                    FloatingActionButton(
                      tooltip: 'Home',
                      onPressed: () => {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => HomePage()))
                      },
                      child: Icon(Icons.home),
                      backgroundColor: Color(0xff00406E).withOpacity(0.50),
                      hoverColor: Colors.white.withOpacity(0.25),
                    ),
                  ],
                ),
              ),
              Container(
                child: ListView(
                  children: <Widget>[
                    alarmItem(_timeString, false),
                    alarmItem(_timeString, true),
                  ],
                ),
              ),
            ],
          ),
        ),
        floatingActionButton: _bottomButtons(),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      ),
    );
  }

  Widget? _bottomButtons() {
    if (_tabController.index == 1) {
      return FloatingActionButton(
        onPressed: () => {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => AddAlarm()))
        },
        backgroundColor: Color(0xff00406E),
        child: Icon(
          Icons.add,
          size: 20.0,
        ),
      );
    } else {
      return null;
    }
  }
}
