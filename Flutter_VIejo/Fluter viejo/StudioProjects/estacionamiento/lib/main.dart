import 'package:flutter/material.dart';

import 'DeliveryScreen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Estimations NQ',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: DeliveryScreen(),
    );
  }
}

class AskScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Estimations NQ'),
      ),
      body: Center(
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Text(
            'Desde: Brown 328',
          ),
          Text(
            'Hasta colihue 1027',
          ),
          FloatingActionButton(
            child: Text('Aceptar estacionamiento'),
            onPressed: () {},
          ),
        ]),
      ),
    );
  }
}
