import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class DeliveryScreen extends StatefulWidget {
  _DeliveryScreenState createScreenState() => _DeliveryScreenState();

  @override
  State<StatefulWidget> createState() {
    throw UnimplementedError();
  }
}

class _DeliveryScreenState extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GoogleMap(
        initialCameraPosition: CameraPosition(
      target: LatLng(0, 0),
      zoom: 12,
    ));
  }
}
