import 'package:flutter/material.dart';
import 'package:estacionamiento/utils/globals.dart' as globals;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Mercado Pago',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage();
  _MyHomePageState createState() => _MyHomePageState;
}

class _MyHomePageState extends State<MyHomePage> {
  Future<Map<String, dynamic>> armarPreferencias() async {
    var Mercado = MP(globals.MercadoClientID, globals.MercadoClientSecret);
    var preference = {
      "items": [
        {
          "title": "Test Modified",
          "quantity": 1,
          "currency_id": "USD",
          "unit_price": 20.4
        }
      ]
    };

    var result = await Mercado.createPreference(preference);

    return result;
  }

  Future<void> ejecutarMercadoPago() async {
    armarPreferencias().then((result) {

      if (result != null) {
       var PreferencesID = result ['response'] ['id'];
        print("resultado ${result.toString()}")
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Mercado Estimations NQ"),
      ),
      body: Center(
        child: MaterialButton(
          color: Colors.blue,
          onPressed: ejecutarMercadoPago,
          child: Text(
            "compra de mercado pago",
            style: TextStyle(color: Colors.white10),
          ),
        ),
      ),
    );
  }
}

class MP {
  MP(String s, String t);

  createPreference(Map<String, List<Map<String, Object>>> preference) {}
}
