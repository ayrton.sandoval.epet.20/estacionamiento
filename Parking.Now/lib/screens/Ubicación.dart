import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:email_password_login/model/user_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'home_screen.dart';
import 'login_screen.dart';

class UbiScreen extends StatefulWidget {
  const UbiScreen({Key? key}) : super(key: key);

  @override
  _UbiScreenState createState() => _UbiScreenState();
}

class _UbiScreenState extends State<UbiScreen> {
  User? user = FirebaseAuth.instance.currentUser;
  UserModel loggedInUser = UserModel();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff00406E),
        title: Text(
          'PARKING NOW',
          style: TextStyle(
            color: Colors.white,
            fontSize: 25,
            fontWeight: FontWeight.bold,
            fontFamily: 'helios',
            letterSpacing: 5,
          ),
        ),
        titleSpacing: 50,
        elevation: 10,
        centerTitle: true,
      ),
      drawer: Drawer(
        child: Container(
          color: Color(0xff00406E),
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              UserAccountsDrawerHeader(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        Color(0xff31AAC1),
                        Color(0xff00406E),
                      ]),
                ),
                accountName: Text(
                  "${loggedInUser.Nombre} ${loggedInUser.Apellido}",
                  style: TextStyle(
                    fontFamily: 'helios',
                  ),
                ),
                accountEmail: Text("${loggedInUser.email}"),
                currentAccountPicture: CircleAvatar(
                  backgroundColor: Colors.white,
                  child: Text(
                    'User',
                    style: TextStyle(fontSize: 20, fontFamily: 'Neo'),
                  ),
                ),
              ),
              ListTile(
                title: const Text(
                  'Cerrar Sesion',
                  style: TextStyle(color: Colors.white),
                ),
                leading: Icon(
                  Icons.outbond,
                  color: Colors.white,
                ),
                onTap: () {
                  logout(context);
                },
              ),
            ],
          ),
        ),
      ),
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color(0xff00406E),
                Color(0xff31AAC1),
              ]),
        ),
        child: Stack(
          children: [
            Align(
              alignment: Alignment.center,
              child: Image.asset(
                '../assets/img/ubi.jpeg',
                scale: 0.01,
              ),
            ),
            Align(
              alignment: Alignment(0.8, 0.6),
              child: FloatingActionButton(
                onPressed: () {},
                tooltip: 'Selecciona la ubicación',
                child: Icon(Icons.location_on),
                backgroundColor: Color(0xff00406E).withOpacity(0.50),
                hoverColor: Colors.white.withOpacity(0.25),
              ),
            ),
            Align(
              alignment: Alignment(0.8, 0.9),
              child: FloatingActionButton(
                tooltip: 'Atrás',
                onPressed: () => {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => HomeScreen()))
                },
                child: Icon(Icons.arrow_left),
                backgroundColor: Color(0xff00406E).withOpacity(0.50),
                hoverColor: Colors.white.withOpacity(0.25),
              ),
            ),
          ],
        ),
      ),
    );
  }

  // the logout function
  Future<void> logout(BuildContext context) async {
    await FirebaseAuth.instance.signOut();
    Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => LoginScreen()));
  }
}
