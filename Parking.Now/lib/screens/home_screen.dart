import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:email_password_login/model/user_model.dart';
import 'package:email_password_login/screens/Alarma.dart';
import 'package:email_password_login/screens/DatosPage.dart';
import 'package:email_password_login/screens/Ubicaci%C3%B3n.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'login_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  User? user = FirebaseAuth.instance.currentUser;
  UserModel loggedInUser = UserModel();

  @override
  void initState() {
    super.initState();
    FirebaseFirestore.instance
        .collection("users")
        .doc(user!.uid)
        .get()
        .then((value) {
      this.loggedInUser = UserModel.fromMap(value.data());
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon:
              Icon(Icons.person_outline, color: Colors.white.withOpacity(0.90)),
          onPressed: () {
            // passing this to our root
            logout(context);
          },
        ),
        backgroundColor: Color(0xff00406E),
        title: Text(
          'PARKING NOW',
          style: TextStyle(
            color: Colors.white,
            fontSize: 25,
            fontWeight: FontWeight.bold,
            fontFamily: 'helios',
            letterSpacing: 5,
          ),
        ),
        titleSpacing: 50,
        elevation: 10,
        centerTitle: true,
      ),
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color(0xff00406E),
                Color(0xff31AAC1),
              ]),
        ),
        child: Stack(
          children: [
            Align(
              alignment: Alignment.center,
              child: Image.asset(
                '../assets/img/ubi.jpeg',
                scale: 1,
              ),
            ),
            Align(
              alignment: Alignment(0.8, 0.3),
              child: FloatingActionButton(
                tooltip: 'Selecciona la ubicación',
                onPressed: () => {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => UbiScreen()))
                },
                child: Icon(Icons.person_pin_circle),
                backgroundColor: Color(0xff00406E).withOpacity(0.50),
                hoverColor: Colors.white.withOpacity(0.25),
              ),
            ),
            Align(
              alignment: Alignment(0.8, 0.6),
              child: FloatingActionButton(
                tooltip: 'Alarma',
                onPressed: () => {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => alarma()))
                },
                child: Icon(Icons.alarm),
                backgroundColor: Color(0xff00406E).withOpacity(0.50),
                hoverColor: Colors.white.withOpacity(0.25),
              ),
            ),
            Align(
              alignment: Alignment(0.8, 0.9),
              child: FloatingActionButton(
                tooltip: 'Pagar',
                onPressed: () => {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => DatosPage()))
                },
                child: Icon(Icons.monetization_on),
                backgroundColor: Color(0xff00406E).withOpacity(0.50),
                hoverColor: Colors.white.withOpacity(0.25),
              ),
            ),
          ],
        ),
      ),
    );
  }

  // the logout function
  Future<void> logout(BuildContext context) async {
    await FirebaseAuth.instance.signOut();
    Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => LoginScreen()));
  }
}
