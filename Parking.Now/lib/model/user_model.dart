class UserModel {
  String? uid;
  String? email;
  String? Nombre;
  String? Apellido;

  UserModel({this.uid, this.email, this.Nombre, this.Apellido});

  // receiving data from server
  factory UserModel.fromMap(map) {
    return UserModel(
      uid: map['uid'],
      email: map['email'],
      Nombre: map['nombre'],
      Apellido: map['apellido'],
    );
  }

  // sending data to our server
  Map<String, dynamic> toMap() {
    return {
      'uid': uid,
      'email': email,
      'nombre': Nombre,
      'apellido': Apellido,
    };
  }
}
